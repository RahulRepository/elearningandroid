package com.tkcs.elearningandroid.fragment;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.FileUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.tkcs.elearningandroid.R;
import com.tkcs.elearningandroid.util.AppUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;

public class HomeFragment extends Fragment {
    private final static int IV_LENGTH = 16;
    private String secreteKey = "Rahul@123";
    private String tag = getClass().getName();
    private View view;
    public static final int FILEPICKER_PERMISSIONS = 1;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        Button selectFileButton = null;
        selectFileButton = view.findViewById(R.id.selectFileButton);

        selectFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                startActivityForResult(intent, 7);

            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        File incryptedFile = null, decyptedFile = null, encryptedFile = null;
        String encryptedFilePath = null, decryptedFilePath;
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(tag, "Inside onActivityResult");
        Log.e(tag, "Result code : " + resultCode);
        switch (requestCode) {

            case 7:
                Log.e(tag, "Inside case");
                if (resultCode == -1) {
                    String PathHolder = data.getData().getPath();

                    String inputFilePath = PathHolder;
                    File inputFileForEncryption = new File(inputFilePath);
                    StringBuffer inputSB = new StringBuffer(inputFilePath);
                    inputSB.insert(inputFilePath.indexOf('.'), "enc");
                    File EncryptedFile = new File(inputSB.toString());
                    encryptedFilePath = PathHolder;
                    Log.e(tag, "Encrypted File Path : " + encryptedFilePath);
                    Toast.makeText(getContext(), PathHolder, Toast.LENGTH_LONG).show();
                    try {
                        byte[] iv = new byte[IV_LENGTH];
                        AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
                        if (encryptedFilePath != null && !(encryptedFilePath.isEmpty())) {
                            incryptedFile = new File(encryptedFilePath);
                            StringBuffer sb = new StringBuffer(encryptedFilePath);
                            sb.insert(encryptedFilePath.indexOf('.'), "dec");
                            //decryptedFilePath = encryptedFilePath.substring(encryptedFilePath.indexOf('.'))+"dec";
                            decryptedFilePath = sb.toString();
                            //encryptedFile = new File(decryptedFilePath);
                            Log.e(tag, "Decrypted File Path : " + decryptedFilePath);
                            decyptedFile = new File(decryptedFilePath);
                           /* if (!decyptedFile.exists()) {
                                decyptedFile.createNewFile();
                            }*/
                        }
                        //AppUtil.encrypt(secreteKey, paramSpec, new FileInputStream(inputFileForEncryption), new FileOutputStream(EncryptedFile));
                        Log.e("Before decrypt", "Before Decrypt");
                        AppUtil.decrypt(secreteKey, paramSpec, new FileInputStream(incryptedFile), new FileOutputStream(decyptedFile));
                        Log.e("After decrypt", "After Decrypt");

                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (InvalidAlgorithmParameterException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                break;
        }
    }
}

