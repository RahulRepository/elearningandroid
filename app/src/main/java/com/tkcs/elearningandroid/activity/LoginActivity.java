package com.tkcs.elearningandroid.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tkcs.elearningandroid.R;
import com.tkcs.elearningandroid.util.AppUtil;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Button loginButton = null;
        EditText pazzword = null, username = null;
        String usernameText = null, pazzwordText = null;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginButton = findViewById(R.id.loginButton);
        username = findViewById(R.id.username);
        pazzword = findViewById(R.id.pazzword);

        final EditText finalUsername = username;
        final EditText finalPazzword = pazzword;
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String usernameText = finalUsername.getText().toString();
                String pazzwordText = finalPazzword.getText().toString();
                if (!(usernameText.isEmpty()) && !(pazzwordText.isEmpty())) {
                    Intent i = new Intent(LoginActivity.this, MainActivity2.class);
                    startActivity(i);
                } else {
                    AppUtil.showSnackBar("Please check username/password", LoginActivity.this);
                }
            }
        });
    }
}